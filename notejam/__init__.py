from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail

# @TODO use application factory approach
application = Flask(__name__)
application.config.from_object('notejam.config.Config')
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(application)

login_manager = LoginManager()
login_manager.login_view = "signin"
login_manager.init_app(application)

mail = Mail()
mail.init_app(application)

from notejam import views
