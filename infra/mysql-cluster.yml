apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql-cluster
  labels:
    app: mysql-cluster
  namespace: #{openshift_project_name}#
data:
  replication.cnf: |
    [mysqld]
    log-bin
    gtid-mode=ON
    enforce-gtid-consistency
    log-slave-updates

---

apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql-cluster-controller-script
  labels:
    app: mysql-cluster
  namespace: #{openshift_project_name}#
data:
  orchestrator.sh: |
    #!/bin/sh

    export mysqlCluster0ip
    export mysqlCluster0role
    export mysqlCluster1ip
    export mysqlCluster1role
    export sourceIP=""
    export replicaIP=""
    export noneIP=""

    #ping localhost

    main() {

            log "MYSQL-CLUSTER ORCHESTRATOR STARTING"

            initFolders

            # log "Waiting for 2 mysqls registered"
            #
            # while [ ! $(cat /failover/status.txt | wc -l) -eq 2 ]
            # do
            #         sleep 1
            # done
            #
            # log "Can now see both mysql nodes in status file"

            getStatus

            #echo "$sourceIP $replicaIP $noneIP" >> /failover/log.log

            #ping localhost

            while :
            do

                    sleep 1

                    ls /failover/fatal > /dev/null 2>&1 && log "Orchestrator gave up on bringing up REPLICA on $noneIP, quitting" && exit 1

                    [ "$replicaIP" = "" ] && getReplicaUp

                    mysql -h $sourceIP -uroot -p#{mysql_password}# -e "SELECT @@hostname;" > /failover/heartbeat.txt || failoverCountdown || doFailover || break

                    ### debug
                    #read -p "After failover done successfully ..."

                    getStatus

            done

            log "ORCHESTRATOR encountered unrecoverable error. EXITING. Please examine the logs."
            exit 1

    }

    failoverCountdown(){
            log "MASTER ($sourceIP) MYSQL PROBE FAILED - STARTING FAILOVER COUNTDOWN"
            sleep 1 && mysql -h $sourceIP -uroot -p#{mysql_password}# -e "SELECT 1;" ||
            sleep 1 && mysql -h $sourceIP -uroot -p#{mysql_password}# -e "SELECT 1;" ||
            sleep 1 && mysql -h $sourceIP -uroot -p#{mysql_password}# -e "SELECT 1;" ||
            sleep 1 && mysql -h $sourceIP -uroot -p#{mysql_password}# -e "SELECT 1;"
    }

    doFailover(){

            log "SOURCE ($sourceIP) mysql check has failed 5 seconds in a row - starting FAILOVER to REPLICA ($replicaIP)"

            sed -i 's/\('"$sourceIP"'\) SOURCE/\1 NONE/g' /failover/status.txt

            log "Marked SOURCE ($sourceIP) NONE to stop application traffic"

            log "Making sure SLAVE_IO and SLAVE_SQL are running on $replicaIP, stopping SLAVE_IO and waiting for relay log to have been read fully..."

            slave_status=$(mysql -h $replicaIP -uroot -p#{mysql_password}# -e "show replica status\G")

            echo "$slave_status" | grep -q "Replica_IO_Running: Yes\|Replica_IO_Running: Connecting" || {
                    log "Slave_IO not running on $replicaIP, restoring traffic to old master and exiting"
                    sed -i 's/\('"$sourceIP"'\) NONE/\1 SOURCE/g' /failover/status.txt
                    exit 1
            }

            echo "$slave_status" | grep -q "Replica_SQL_Running: Yes" || {
                    log "Slave_SQL not running on $replicaIP, restoring traffic to old master and exiting"
                    sed -i 's/\('"$sourceIP"'\) NONE/\1 SOURCE/g' /failover/status.txt
                    exit 1
            }

            ### Stopping IO thread

            mysql -h $replicaIP -uroot -p#{mysql_password}# -e "stop replica io_thread;" || {
                    log "Failed to stop replica io_thread on $replicaIP, restoring traffic to old master and exiting"
                    sed -i 's/\('"$sourceIP"'\) NONE/\1 SOURCE/g' /failover/status.txt
                    exit 1
            }

            ### Waiting for full consumption of relay log

            echo $(mysql -h $replicaIP -uroot -p#{mysql_password}# -e "show processlist") | grep -q "has read all relay log" ||
            sleep 5 && echo $(mysql -h $replicaIP -uroot -p#{mysql_password}# -e "show processlist") | grep -q "has read all relay log" ||
            sleep 5 && echo $(mysql -h $replicaIP -uroot -p#{mysql_password}# -e "show processlist") | grep -q "has read all relay log" ||
            sleep 5 && echo $(mysql -h $replicaIP -uroot -p#{mysql_password}# -e "show processlist") | grep -q "has read all relay log" ||
            sleep 5 && echo $(mysql -h $replicaIP -uroot -p#{mysql_password}# -e "show processlist") | grep -q "has read all relay log" || {
                    log "Replica ($replicaIP) was not able to read all relay log within 4x5 seconds, restoring traffic to old master and exiting"
                    sed -i 's/\('"$sourceIP"'\) NONE/\1 SOURCE/g' /failover/status.txt
                    exit 1
            }


            log "...DONE"

            log "Attempting to stop REPLICA and reset master on $replicaIP..."

            mysql -h $replicaIP -uroot -p#{mysql_password}# -e "stop replica;" && mysql -h $replicaIP -uroot -p#{mysql_password}# -e "reset master;"

            [ $? -eq 0 ] || {
                    log "One of -STOP REPLICA-/-RESET MASTER- commands failed on $replicaIP, restoring traffic to old master and exiting"
                    sed -i 's/\('"$sourceIP"'\) NONE/\1 SOURCE/g' /failover/status.txt
                    exit 1
            }

            log "...DONE"

            log "Attempting to reset REPLICA on current SOURCE ($replicaIP)..."

            mysql -h $replicaIP -uroot -p#{mysql_password}# -e "reset replica;"

            [ $? -eq 0 ] || {
                    log "RESET REPLICA command failed on $replicaIP, restoring traffic to old master and exiting"
                    sed -i 's/\('"$sourceIP"'\) NONE/\1 SOURCE/g' /failover/status.txt
                    exit 1
            }

            log "...DONE"

            sed -i 's/\('"$replicaIP"'\) REPLICA/\1 SOURCE/g' /failover/status.txt

            replicaIP=""

            # return 1

    }

    getReplicaUp(){

            log "Waiting for secondary mysql ($noneIP) to accept connections"

            c=0

            while [ ! $(mysql -h $noneIP -uroot -p#{mysql_password}# -e "SELECT 1;") ]
            do
                    sleep 1
                    getStatus
                    let "c+=1"
                    [ $c -gt 300 ] && {
                            log "Timeout reached while waiting for response from REPLICA, not trying any more"
                            touch /failover/fatal
                            exit 0
                    }
            done

            log "Can query secondary mysql ($noneIP) successfully"

            bad_slave_status=$(mysql -h $noneIP -uroot -p#{mysql_password}# -e "show replica status\G")

            echo "$bad_slave_status" | grep -q "Replica_IO_Running: Yes\|Replica_IO_Running: Connecting" && {
                    log "Slave_IO already running on $noneIP, must indicate a partially failed attempt to start replication, exiting"
                    exit 1
            }

            echo "$bad_slave_status" | grep -q "Replica_SQL_Running: Yes" && {
                    log "Slave_SQL already running on $noneIP, must indicate a partially failed attempt to start replication, exiting"
                    exit 1
            }

            log "Attempting to get replication running on $noneIP..."

            mysql -h $noneIP -uroot -p#{mysql_password}# -e "CHANGE REPLICATION SOURCE TO SOURCE_HOST = '$sourceIP', SOURCE_PORT = 3306, SOURCE_USER = 'root', SOURCE_PASSWORD = '#{mysql_password}#', SOURCE_AUTO_POSITION = 1;" && mysql -h $noneIP -uroot -p#{mysql_password}# -e "start replica;"

            [ $? -eq 0 ] || {
                    log "One of -CHANGE REPLICATION SOURCE-/-START REPLICA- commands failed on $noneIP, exiting"
                    exit 1
            }

            sed -i 's/\('"$replicaIP"'\) NONE/\1 REPLICA/g' /failover/status.txt

            noneIP=""

            log "...DONE"

            # echo bla
            # return 1
    }

    log(){
            echo $(TZ=EET-2 date +"%Y%m%d %H:%M:%S EET-2") --- $1 >> /failover/orchestrator_logs/$(TZ=EET-2 date +"%Y%m%d")-log.txt
    }

    getStatus(){

            updateIps

            mysqlCluster0ip=$(grep mysql-cluster-0 /failover/status.txt | cut -d" " -f2)
            mysqlCluster0role=$(grep mysql-cluster-0 /failover/status.txt | cut -d" " -f3)
            mysqlCluster1ip=$(grep mysql-cluster-1 /failover/status.txt | cut -d" " -f2)
            mysqlCluster1role=$(grep mysql-cluster-1 /failover/status.txt | cut -d" " -f3)

            ###debug

            #echo "mysqlCluster0ip: $mysqlCluster0ip, mysqlCluster0role: $mysqlCluster0role, mysqlCluster1ip: $mysqlCluster1ip, mysqlCluster1role: $mysqlCluster1role,"

            if [ "$mysqlCluster0role" = "SOURCE" ]
            then
                    sourceIP=$mysqlCluster0ip
            elif [ "$mysqlCluster1role" = "SOURCE" ]
            then
                    sourceIP=$mysqlCluster1ip
            fi

            if [ "$mysqlCluster0role" = "REPLICA" ]
            then
                    replicaIP=$mysqlCluster0ip
            elif [ "$mysqlCluster1role" = "REPLICA" ]
            then
                    replicaIP=$mysqlCluster1ip
            fi

            if [ "$mysqlCluster0role" = "NONE" ]
            then
                    noneIP=$mysqlCluster0ip
            elif [ "$mysqlCluster1role" = "NONE" ]
            then
                    noneIP=$mysqlCluster1ip
            fi


    }

    updateIps(){

            ### Wait until initial SOURCE pod registers itself

            while [ ! $(cat /failover/mysql-cluster-0.status | wc -l) -eq 1 ]
            do
                    sleep 1
                    log "Waiting for initial SOURCE mysql pod to register ip..."
            done

            ### Get the ip

            ip0="$(grep mysql-cluster-0 /failover/mysql-cluster-0.status | cut -d" " -f"2")"


            ### Update mysql-cluster-0 ip if it exists in status.txt or create initial SOURCE entry

            grep -sq "mysql-cluster-0 $ip0" /failover/status.txt || {
                    grep -sq mysql-cluster-0 /failover/status.txt && \
                    sed -i 's/\(mysql-cluster-0\) \([^ ]*\)/\1 '"$ip0"'/g' /failover/status.txt || \
                    echo "mysql-cluster-0 $ip0 SOURCE" >> /failover/status.txt
            }

            ### Wait until initial NONE pod registers itself

            while [ ! $(cat /failover/mysql-cluster-1.status | wc -l) -eq 1 ]
            do
                    sleep 1
                    log "Waiting for initial NONE mysql pod to register ip..."
            done

            ### Get the ip

            ip1="$(grep mysql-cluster-1 /failover/mysql-cluster-1.status | cut -d" " -f"2")"

            ### Update initial mysql-cluster-1 ip if it exists in status.txt or create initial NONE entry

            grep -sq "mysql-cluster-1 $ip1" /failover/status.txt || {
                    grep -sq mysql-cluster-1 /failover/status.txt && \
                    sed -i 's/\(mysql-cluster-1\) \([^ ]*\)/\1 '"$ip1"'/g' /failover/status.txt || \
                    echo "mysql-cluster-1 $ip1 NONE" >> /failover/status.txt
            }

    }

    initFolders(){

            ls /failover/restarts || mkdir /failover/restarts
            ls /failover/orchestrator_logs || mkdir /failover/orchestrator_logs

    }

    main "$@"; exit


---

apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql-cluster-cron-script
  labels:
    app: mysql-cluster
  namespace: #{openshift_project_name}#
data:
  cron.sh: |
    #!/bin/sh

    ### this will purge the logs older than 50 days every day at 14:59
    while :
    do
            sleep 60
            ls /failover/orchestrator_logs > /dev/null 2>&1 && [ $(date +"%H%M") == 1459 ] && find /failover/orchestrator_logs/ -type f -mtime +50 -name '*log.txt' -exec rm -- '{}' \;
    done

---

apiVersion: v1
kind: Service
metadata:
  name: mysql-cluster
  labels:
    app: mysql-cluster
spec:
  ports:
    - name: mysql-cluster
      port: 3306
      nodePort: 32701
      targetPort: 3306
  selector:
    app: mysql-cluster
  type: LoadBalancer

---

apiVersion: route.openshift.io/v1
kind: Route
metadata:
  labels:
    service: mysql-cluster
  name: mysql-cluster
spec:
  host: mysql-cluster-#{openshift_project_name}#.paas.dpd.baltic
  port:
    targetPort: 3306
  to:
    kind: Service
    name: mysql-cluster
    weight: 100
status:
  ingress:
  - conditions:
    host: mysql-cluster-#{openshift_project_name}#.paas.dpd.baltic
    routerName: router
---

apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-cluster-failover-data-persistence
  annotations:
    volume.beta.kubernetes.io/storage-class: glusterfs-storage
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 50Mi
  storageClassName: glusterfs-storage

---

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mysql-cluster
spec:
  selector:
    matchLabels:
      app: mysql-cluster
  serviceName: mysql-cluster
  replicas: 2
  template:
    metadata:
      labels:
        app: mysql-cluster
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: environment
                operator: In
                values:
                - #{taint_environment_value}#
      initContainers:
      - name: init-mysql
        image: docker-registry.default.svc:5000/#{openshift_project_name}#/mojo.mysql:8.0.23
        imagePullPolicy: IfNotPresent
        command:
        - bash
        - "-c"
        - |
          set -ex
          # Generate mysql server-id from pod ordinal index.
          [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
          ordinal=${BASH_REMATCH[1]}
          echo [mysqld] > /mnt/conf.d/server-id.cnf
          # Add an offset to avoid reserved server-id=0 value.
          echo server-id=$((100 + $ordinal)) >> /mnt/conf.d/server-id.cnf
          # Copy conf
          cp /mnt/config-map/replication.cnf /mnt/conf.d/
        volumeMounts:
        - name: conf
          mountPath: /mnt/conf.d
        - name: config-map
          mountPath: /mnt/config-map
      containers:
      - name: mysql
        args:
          - --default-authentication-plugin=mysql_native_password
        image: docker-registry.default.svc:5000/#{openshift_project_name}#/mojo.mysql:8.0.23
        imagePullPolicy: IfNotPresent
        env:
        # - name: TZ
        #   value: Europe/Vilnius
        # - name: MYSQL_DATABASE
        #   value: #{mysql_database_name}#
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: mysql_password
              name: mysql-secret
        # - name: MYSQL_USER
        #   value: #{mysql_username}#
        # - name: MYSQL_PASSWORD
        #   valueFrom:
        #     secretKeyRef:
        #       key: mysql_user_password
        #       name: mysql-secret
        lifecycle:
          postStart:
            exec:
              command:
              - /bin/sh
              - "-c"
              - |
                grep -sq $(hostname) /failover/"$(hostname)".status && \
                sed -i 's/\('"$(hostname)"'\) \([^ ]*\)/\1 '"$(hostname -i)"'/g' /failover/"$(hostname)".status || \
                (if [ "$(hostname)" = "mysql-cluster-0" ]; then echo "$(hostname) $(hostname -i) SOURCE" > /failover/"$(hostname)".status; \
                else echo "$(hostname) $(hostname -i) NONE" > /failover/"$(hostname)".status; fi)
          preStop:
            exec:
              command:
              - /bin/sh
              - "-c"
              - |
                ls /failover/restarts/$(hostname) > /dev/null 2>&1 && rm /failover/restarts/$(hostname)
        ports:
        - name: mysql
          containerPort: 3306
        volumeMounts:
        - name: data
          mountPath: /var/lib/mysql
          subPath: mysql
        - name: conf
          mountPath: /etc/mysql/conf.d
        - name: mysql-cluster-failover-data-persistence
          mountPath: /failover
        resources:
          limits:
            cpu: 200m
            memory: 1Gi
          requests:
            cpu: 100m
            memory: 500Mi
        livenessProbe:
          exec:
            command: ["/bin/bash", "-c", "[[ ! -f /failover/restarts/$(hostname) ]]"]
          initialDelaySeconds: 5
          periodSeconds: 1
          timeoutSeconds: 3
          failureThreshold: 1
        readinessProbe:
          exec:
            # Check we can execute queries over TCP (skip-networking is off).
            command: ["/bin/bash", "-c", "grep $(hostname) /failover/status.txt | grep \" SOURCE$\""]
            # command: ["mysql", "-h", "127.0.0.1", "-uroot", "-p#{mysql_password}#", "-e", "SELECT 1"]
            # command: ["mysql", "-h", "127.0.0.1", "-uroot", "-e", "SELECT 1"]
          initialDelaySeconds: 60
          periodSeconds: 1
          timeoutSeconds: 3
          failureThreshold: 1
      # - name: mysql-client
      #   image: docker-registry.default.svc:5000/#{openshift_project_name}#/mojo.mysql-client:1.0
      #   imagePullPolicy: IfNotPresent
      #   command: ["ping"]
      #   args: ["localhost"]
      #   resources:
      #     limits:
      #       cpu: 50m
      #       memory: 100Mi
      #     requests:
      #       cpu: 25m
      #       memory: 50Mi
      restartPolicy: Always
      volumes:
      - name: conf
        emptyDir: {}
      - name: config-map
        configMap:
          name: mysql-cluster
      - name: mysql-cluster-failover-data-persistence
        persistentVolumeClaim:
          claimName: mysql-cluster-failover-data-persistence
  volumeClaimTemplates:
  - metadata:
      name: data
    spec:
      accessModes: ["ReadWriteOnce"]
      resources:
        requests:
          storage: 1Gi

---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql-cluster-controller-deployment
  labels:
    app: mysql-cluster-controller
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql-cluster-controller
  template:
    metadata:
      labels:
        app: mysql-cluster-controller
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: environment
                operator: In
                values:
                - #{taint_environment_value}#
      containers:
      - name: mysql-cluster-controller
        image: docker-registry.default.svc:5000/#{openshift_project_name}#/mojo.mysql-client:1.0
        imagePullPolicy: IfNotPresent
        command: ["/scripts/orchestrator.sh"]
        #args: ["localhost"]
        resources:
          limits:
            cpu: 50m
            memory: 100Mi
          requests:
            cpu: 25m
            memory: 50Mi
        volumeMounts:
        - name: mysql-cluster-failover-data-persistence
          mountPath: /failover
        - name: orchestrator
          mountPath: /scripts
      - name: mysql-cluster-log-rotation
        image: docker-registry.default.svc:5000/#{openshift_project_name}#/mojo.mysql-client:1.0
        imagePullPolicy: IfNotPresent
        command: ["/log_rotation/cron.sh"]
        resources:
          limits:
            cpu: 50m
            memory: 100Mi
          requests:
            cpu: 25m
            memory: 50Mi
        volumeMounts:
        - name: mysql-cluster-failover-data-persistence
          mountPath: /failover
        - name: cron
          mountPath: /log_rotation
      restartPolicy: Always
      volumes:
      - name: mysql-cluster-failover-data-persistence
        persistentVolumeClaim:
          claimName: mysql-cluster-failover-data-persistence
      - name: orchestrator
        configMap:
          name: mysql-cluster-controller-script
          defaultMode: 0740
      - name: cron
        configMap:
          name: mysql-cluster-cron-script
          defaultMode: 0740

---

apiVersion: v1
kind: Secret
metadata:
  name: mysql-secret
stringData:
  mysql_password: #{mysql_password}#
  mysql_user_password: #{mysql_user_password}#
type: Opaque
